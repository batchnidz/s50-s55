import React from 'react';
import { NavLink } from 'react-router-dom';

const PageNotFound = () => {
  return (
    <div>
      <h1 className="pagetitle">Page Not Found</h1>
      <p>Go Back to <NavLink  className="notfound" to="/">homepage</NavLink>.</p>
    </div>
  );
};

export default PageNotFound;